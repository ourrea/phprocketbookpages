<?php

class RocketPages {

    protected $types = [
        '0' => [
            "DotGrid",
            [
                'A4' => '5',
                'Letter' => '4'
            ]
        ],
        '1' => [
            "Graph" => [
                'A4' => 'P', 'Letter' => 'O']], '2' => ["Lined" => ['A4' => 'N', 'Letter' => 'M']], '3' => ["Music" => ['A4' => 'R', 'Letter' => 'Q']]];

}
